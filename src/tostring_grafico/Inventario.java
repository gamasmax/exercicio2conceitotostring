/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tostring_grafico;

/**
 *
 * @author Aluno
 */
public class Inventario {
    private int limite;
    private String[] inventario;
    private String capacete;
    private String peitoral;
    private String botas;
    private String anel;
    private String espada;
    
    public void removeCapacete(){
        this.capacete="";
    }
    public void removePeitoral(){
        this.peitoral="";
    }
    public void removeBotas(){
        this.botas="";
    }

    public void removeAnel(){
        this.anel="";
    }
    
    public String[] getInventario() {
        return inventario;
    }

    public void setInventario(String[] inventario) {
        this.inventario = inventario;
    }

    public String getAnel() {
        return anel;
    }

    public void setAnel(String anel) {
        this.anel = anel;
    }
    
    public Inventario(int limite) {
        this.limite = limite;
        inventario = new String[limite];
        for (int i=0; i<limite; i++){
            inventario[i]="";
        }
        this.capacete="";
        this.peitoral="";
        this.botas="";
        this.anel="";
    }
    public void addItem(int index, String nome){
        if (index>=0&&index<this.limite){
            if ("".equals(this.inventario[index])){
               this.inventario[index]=nome; 
            }   
        }
    }
    public void removeItem(int index){
       if (index>=0&&index<this.limite){
            if (!"".equals(this.inventario[index])){
               this.inventario[index]=""; 
            }   
        } 
    }
    /*
    @Override
    public String toString(){
        String string = "Espada: " + espada + "\nCapacete: " + capacete + "\nArmadura: " + armadura + "\nBotas: " + botas + "\nLuvas: " + luvas + "\nAnel: " + anel;
        string+= "\nInventário:\n";
        for (int i=0; i<this.limite; i++){
            if (this.inventario[i]!=""){
            string+= i+1 + ": " + this.inventario[i] + "\n";
            }
            else{
                string+= i+1 + ": \n";
            }
        }
        return string;
    }
*/


    public int getLimite() {
        return limite;
    }

    public void setLimite(int limite) {
        this.limite = limite;
    }

    public String getCapacete() {
        return capacete;
    }

    public void setCapacete(String capacete) {
        this.capacete = capacete;
    }

    public String getPeitoral() {
        return peitoral;
    }

    public void setPeitoral(String peitoral) {
        this.peitoral = peitoral;
    }

    public String getBotas() {
        return botas;
    }

    public void setBotas(String botas) {
        this.botas = botas;
    }

    public String getEspada() {
        return espada;
    }

    public void setEspada(String espada) {
        this.espada = espada;
    }
    
}
