/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tostring_grafico;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Raynan
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private TextField nome1;
    @FXML
    private TextField vida1;
    @FXML
    private TextField mana1;
    @FXML
    private TextField nome2;
    @FXML
    private TextField vida2;
    @FXML
    private TextField mana2;
    @FXML
    private TextField magia11;
    @FXML
    private TextField magia12;
    @FXML
    private TextField magia13;
    @FXML
    private TextField magia14;
    @FXML
    private TextField magia21;
    @FXML
    private TextField magia22;
    @FXML
    private TextField magia23;
    @FXML
    private TextField magia24;
    @FXML
    private TextField custo11;
    @FXML
    private TextField custo12;
    @FXML
    private TextField custo13;
    @FXML
    private TextField custo14;
    @FXML
    private TextField custo21;
    @FXML
    private TextField custo22;
    @FXML
    private TextField custo23;
    @FXML
    private TextField custo24;
    
    @FXML
    private TextField capacete1;
    @FXML
    private TextField capacete2;
    
    @FXML
    private TextField peitoral1;
    @FXML
    private TextField peitoral2;
    
    @FXML
    private TextField anel1;
    @FXML
    private TextField anel2;
    
    @FXML
    private TextField espada1;
    @FXML
    private TextField espada2;
    
    @FXML
    private TextField botas1;
    @FXML
    private TextField botas2;
    
    @FXML
    private TextField invtLim11;
    @FXML
    private TextField invtLim12;
    @FXML
    private TextField invtLim13;
    @FXML
    private TextField invtLim14;
    
    @FXML
    private TextField invtLim21;
    @FXML
    private TextField invtLim22;
    @FXML
    private TextField invtLim23;
    @FXML
    private TextField invtLim24;
    
    Jogador jogador1 = new Jogador(4);
    Jogador jogador2 = new Jogador(4);
    @FXML
    private void atualizar1(ActionEvent event) {
        jogador1.setNome(nome1.getText());
        jogador1.setVida(Integer.parseInt(vida1.getText()));
        jogador1.setMana(Integer.parseInt(mana1.getText()));
    }
    @FXML
    private void atualizar2(ActionEvent event) {
        jogador2.setNome(nome2.getText());
        jogador2.setVida(Integer.parseInt(vida2.getText()));
        jogador2.setMana(Integer.parseInt(mana2.getText()));
    }
    @FXML
    private void MagiaAdc11(ActionEvent event){
        jogador1.getMagias()[0].setNome(magia11.getText());
        jogador1.getMagias()[0].setConsumoDeMana(Integer.parseInt(custo11.getText()));
    }
    @FXML
    private void Magiaremover11(ActionEvent event){
        jogador1.getMagias()[0].setNome("");
        jogador1.getMagias()[0].setConsumoDeMana(0);
        magia11.setText("");
        custo11.setText("0");
    }
    @FXML
    private void MagiaAdc12(ActionEvent event){
        jogador1.getMagias()[1].setNome(magia12.getText());
        jogador1.getMagias()[1].setConsumoDeMana(Integer.parseInt(custo12.getText()));
    }
    @FXML
    private void Magiaremover12(ActionEvent event){
        jogador1.getMagias()[1].setNome("");
        jogador1.getMagias()[1].setConsumoDeMana(0);
        magia12.setText("");
        custo12.setText("0");
    }
    @FXML
    private void MagiaAdc13(ActionEvent event){
        jogador1.getMagias()[2].setNome(magia13.getText());
        jogador1.getMagias()[2].setConsumoDeMana(Integer.parseInt(custo13.getText()));
    }
    @FXML
    private void Magiaremover13(ActionEvent event){
        jogador1.getMagias()[2].setNome("");
        jogador1.getMagias()[2].setConsumoDeMana(0);
        magia13.setText("");
        custo13.setText("0");
    }
    @FXML
    private void MagiaAdc14(ActionEvent event){
        jogador1.getMagias()[3].setNome(magia14.getText());
        jogador1.getMagias()[3].setConsumoDeMana(Integer.parseInt(custo14.getText()));
    }
    @FXML
    private void Magiaremover14(ActionEvent event){
        jogador1.getMagias()[3].setNome("");
        jogador1.getMagias()[3].setConsumoDeMana(0);
        magia14.setText("");
        custo14.setText("0");
    }
    
    @FXML
    private void MagiaAdc21(ActionEvent event){
        jogador2.getMagias()[0].setNome(magia21.getText());
        jogador2.getMagias()[0].setConsumoDeMana(Integer.parseInt(custo21.getText()));
    }
    @FXML
    private void Magiaremover21(ActionEvent event){
        jogador2.getMagias()[0].setNome("");
        jogador2.getMagias()[0].setConsumoDeMana(0);
        magia21.setText("");
        custo21.setText("0");
    }
    @FXML
    private void MagiaAdc22(ActionEvent event){
        jogador2.getMagias()[1].setNome(magia22.getText());
        jogador2.getMagias()[1].setConsumoDeMana(Integer.parseInt(custo22.getText()));
    }
    @FXML
    private void Magiaremover22(ActionEvent event){
        jogador2.getMagias()[1].setNome("");
        jogador2.getMagias()[1].setConsumoDeMana(0);
        magia22.setText("");
        custo22.setText("0");
    }
    @FXML
    private void MagiaAdc23(ActionEvent event){
        jogador2.getMagias()[2].setNome(magia23.getText());
        jogador2.getMagias()[2].setConsumoDeMana(Integer.parseInt(custo23.getText()));
    }
    @FXML
    private void Magiaremover23(ActionEvent event){
        jogador2.getMagias()[2].setNome("");
        jogador2.getMagias()[2].setConsumoDeMana(0);
        magia23.setText("");
        custo23.setText("0");
    }
    @FXML
    private void MagiaAdc24(ActionEvent event){
        jogador2.getMagias()[3].setNome(magia24.getText());
        jogador2.getMagias()[3].setConsumoDeMana(Integer.parseInt(custo24.getText()));
    }
    @FXML
    private void Magiaremover24(ActionEvent event){
        jogador2.getMagias()[3].setNome("");
        jogador2.getMagias()[3].setConsumoDeMana(0);
        magia24.setText("");
        custo24.setText("0");
    }
    
    @FXML
    private void atualizarMagia1(ActionEvent event){
        magia11.setText(jogador1.getMagias()[0].getNome());
        custo11.setText(String.valueOf(jogador1.getMagias()[0].getConsumoDeMana()));
        magia12.setText(jogador1.getMagias()[1].getNome());
        custo12.setText(String.valueOf(jogador1.getMagias()[1].getConsumoDeMana()));
        magia13.setText(jogador1.getMagias()[2].getNome());
        custo13.setText(String.valueOf(jogador1.getMagias()[2].getConsumoDeMana()));
        magia14.setText(jogador1.getMagias()[3].getNome());
        custo14.setText(String.valueOf(jogador1.getMagias()[3].getConsumoDeMana()));
    }
    @FXML
    private void atualizarMagia2(ActionEvent event){
        magia21.setText(jogador2.getMagias()[0].getNome());
        custo21.setText(String.valueOf(jogador2.getMagias()[0].getConsumoDeMana()));
        magia22.setText(jogador2.getMagias()[1].getNome());
        custo22.setText(String.valueOf(jogador2.getMagias()[1].getConsumoDeMana()));
        magia23.setText(jogador2.getMagias()[2].getNome());
        custo23.setText(String.valueOf(jogador2.getMagias()[2].getConsumoDeMana()));
        magia24.setText(jogador2.getMagias()[3].getNome());
        custo24.setText(String.valueOf(jogador2.getMagias()[3].getConsumoDeMana()));
    }
    @FXML
    private void capacete1Adc(ActionEvent event){
        jogador1.getInventarioLimitado().setCapacete(capacete1.getText());
        System.out.println("Item " + capacete1.getText() + " Equipado com sucesso");
    }
    @FXML
    private void capacete1Rem(ActionEvent event){
        jogador1.getInventarioLimitado().setCapacete(""); 
        System.out.println("Item Removido com sucesso");
    }
    @FXML
    private void peitoral1Adc(ActionEvent event){
        jogador1.getInventarioLimitado().setPeitoral(peitoral1.getText()); 
        System.out.println("Item " + peitoral1.getText() + " Equipado com sucesso");
    }
    @FXML
    private void peitoral1Rem(ActionEvent event){
        jogador1.getInventarioLimitado().setPeitoral(""); 
        System.out.println("Item Removido com sucesso");
    }
    @FXML
    private void anel1Adc(ActionEvent event){
        jogador1.getInventarioLimitado().setAnel(anel1.getText()); 
        System.out.println("Item " + anel1.getText() + " Equipado com sucesso");
    }
    @FXML
    private void anel1Rem(ActionEvent event){
        jogador1.getInventarioLimitado().setAnel(""); 
        System.out.println("Item Removido com sucesso");
    }
    @FXML
    private void espada1Adc(ActionEvent event){
        jogador1.getInventarioLimitado().setEspada(espada1.getText()); 
        System.out.println("Item " + espada1.getText() + " Equipado com sucesso");
    }
    @FXML
    private void espada1Rem(ActionEvent event){
        jogador1.getInventarioLimitado().setEspada(""); 
        System.out.println("Item Removido com sucesso");
    }
    @FXML
     private void botas1Adc(ActionEvent event){
        jogador1.getInventarioLimitado().setBotas(botas1.getText()); 
        System.out.println("Item " + botas1.getText() + " Equipado com sucesso");
    }
    @FXML
    private void botas1Rem(ActionEvent event){
        jogador1.getInventarioLimitado().setBotas(""); 
        System.out.println("Item Removido com sucesso");
    }

    @FXML
    private void capacete2Adc(ActionEvent event){
        jogador2.getInventarioLimitado().setCapacete(capacete2.getText());
        System.out.println("Item " + capacete2.getText() + " Equipado com sucesso");
    }
    @FXML
    private void capacete2Rem(ActionEvent event){
        jogador2.getInventarioLimitado().setCapacete(""); 
        System.out.println("Item Removido com sucesso");
    }
    @FXML
    private void peitoral2Adc(ActionEvent event){
        jogador2.getInventarioLimitado().setPeitoral(peitoral2.getText()); 
        System.out.println("Item " + peitoral2.getText() + " Equipado com sucesso");
    }
    @FXML
    private void peitoral2Rem(ActionEvent event){
        jogador2.getInventarioLimitado().setPeitoral(""); 
        System.out.println("Item Removido com sucesso");
    }
    @FXML
    private void anel2Adc(ActionEvent event){
        jogador2.getInventarioLimitado().setAnel(anel2.getText()); 
        System.out.println("Item " + anel2.getText() + " Equipado com sucesso");
    }
    @FXML
    private void anel2Rem(ActionEvent event){
        jogador2.getInventarioLimitado().setAnel(""); 
        System.out.println("Item Removido com sucesso");
    }
    @FXML
    private void espada2Adc(ActionEvent event){
        jogador2.getInventarioLimitado().setEspada(espada2.getText()); 
        System.out.println("Item " + espada2.getText() + " Equipado com sucesso");
    }
    @FXML
    private void espada2Rem(ActionEvent event){
        jogador2.getInventarioLimitado().setEspada(""); 
        System.out.println("Item Removido com sucesso");
    }
    @FXML
     private void botas2Adc(ActionEvent event){
        jogador2.getInventarioLimitado().setBotas(botas2.getText()); 
        System.out.println("Item " + botas2.getText() + " Equipado com sucesso");
    }
    @FXML
    private void botas2Rem(ActionEvent event){
        jogador2.getInventarioLimitado().setBotas(""); 
        System.out.println("Item Removido com sucesso");
    }
    
    
    
    @FXML
    private void remInv11(ActionEvent event){
        jogador1.getInventarioLimitado().removeItem(0);
        invtLim11.setText("");
        System.out.println("Item removido com sucesso");
    }
    @FXML
    private void adcInv11(ActionEvent event){
        jogador1.getInventarioLimitado().addItem(0, invtLim11.getText());
        System.out.println("Item adicionado com sucesso");
    }
    @FXML
    private void remInv12(ActionEvent event){
        jogador1.getInventarioLimitado().removeItem(1);
        invtLim12.setText("");
        System.out.println("Item removido com sucesso");
    }
    @FXML
    private void adcInv12(ActionEvent event){
        jogador1.getInventarioLimitado().addItem(1, invtLim12.getText());
        System.out.println("Item adicionado com sucesso");
    }
    @FXML
    private void remInv13(ActionEvent event){
        jogador1.getInventarioLimitado().removeItem(2);
        invtLim13.setText("");
        System.out.println("Item removido com sucesso");
    }
    @FXML
    private void adcInv13(ActionEvent event){
        jogador1.getInventarioLimitado().addItem(2, invtLim13.getText());
        System.out.println("Item adicionado com sucesso");
    }
    @FXML
    private void remInv14(ActionEvent event){
        jogador1.getInventarioLimitado().removeItem(3);
        invtLim14.setText("");
        System.out.println("Item removido com sucesso");
    }
    @FXML
    private void adcInv14(ActionEvent event){
        jogador1.getInventarioLimitado().addItem(4, invtLim14.getText());
        System.out.println("Item adicionado com sucesso");
    }
    
    
    
     @FXML
    private void remInv21(ActionEvent event){
        jogador2.getInventarioLimitado().removeItem(0);
        invtLim21.setText("");
        System.out.println("Item removido com sucesso");
    }
    @FXML
    private void adcInv21(ActionEvent event){
        jogador2.getInventarioLimitado().addItem(0, invtLim21.getText());
        System.out.println("Item adicionado com sucesso");
    }
    @FXML
    private void remInv22(ActionEvent event){
        jogador2.getInventarioLimitado().removeItem(1);
        invtLim22.setText("");
        System.out.println("Item removido com sucesso");
    }
    @FXML
    private void adcInv22(ActionEvent event){
        jogador2.getInventarioLimitado().addItem(1, invtLim22.getText());
        System.out.println("Item adicionado com sucesso");
    }
    @FXML
    private void remInv23(ActionEvent event){
        jogador2.getInventarioLimitado().removeItem(2);
        invtLim23.setText("");
        System.out.println("Item removido com sucesso");
    }
    @FXML
    private void adcInv23(ActionEvent event){
        jogador2.getInventarioLimitado().addItem(2, invtLim23.getText());
        System.out.println("Item adicionado com sucesso");
    }
    @FXML
    private void remInv24(ActionEvent event){
        jogador2.getInventarioLimitado().removeItem(3);
        invtLim24.setText("");
        System.out.println("Item removido com sucesso");
    }
    @FXML
    private void adcInv24(ActionEvent event){
        jogador1.getInventarioLimitado().addItem(4, invtLim24.getText());
        System.out.println("Item adicionado com sucesso");
    }
       

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
