/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tostring_grafico;

/**
 *
 * @author Aluno
 */
public class Magia {
    private String nome="";
    private int consumoDeMana=0;

    public Magia(String nome, int consumoDeMana) {
        this.nome = nome;
        this.consumoDeMana = consumoDeMana;
    }
    public Magia(){
        
    }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getConsumoDeMana() {
        return consumoDeMana;
    }

    public void setConsumoDeMana(int consumoDeMana) {
        this.consumoDeMana = consumoDeMana;
    }

    @Override
    public String toString() {
        return "Nome da Magia: " + nome + ", consumo de Mana: " + consumoDeMana + "\n";
    }
   
}
