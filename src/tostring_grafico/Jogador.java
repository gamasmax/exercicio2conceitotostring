package tostring_grafico;

public class Jogador {
    private int vida;
    private int mana;
    private String nome;
    private Magia[] magias = new Magia[4];
    private Inventario inventarioLimitado;

    public Jogador(int vida, int mana, String nome) {
        this.vida = vida;
        this.mana = mana;
        this.nome = nome;
        
    }
    public Jogador(int limite){
        inventarioLimitado = new Inventario(limite);
        for (int i=0; i<4; i++){
            magias[i]=new Magia();
        }
    }
    public void executarMagia (String Nome){
        for (int i=0; i<magias.length; i++){
           if(magias[i].getNome()==Nome){
               if(magias[i].getConsumoDeMana()<=mana){
                   mana=mana-magias[i].getConsumoDeMana();
                   System.out.println("A Magia " + magias[i].getNome() + " foi utilizada, mana atual: " + mana);
               }
           }
        }
    }
    public String mostarInventario (){
        return this.inventarioLimitado.toString();
    }
    public int getVida() {
        return vida;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }

    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Magia[] getMagias() {
        return magias;
    }



    public void setMagias(Magia[] Magias) {
        this.magias = Magias;
    }

    public Inventario getInventarioLimitado() {
        return inventarioLimitado;
    }

    public void setInventarioLimitado(Inventario InventarioLimitado) {
        this.inventarioLimitado = InventarioLimitado;
    }
    
}
